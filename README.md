# CD4Analysis Listing

CD4Analysis language definition package for the LaTeX listing package

## Usage

1. Copy both packages to your project
2. Import the following packages:

```latex
\usepackage{zi4}
\usepackage{lstautogobble}
\usepackage{cd4a-listings}
```

3. Define a language style as follows:

```latex
\lstdefinestyle{cdastyle} {
    language=CD4A,
    autogobble,
    columns=fullflexible,
    showspaces=false,
    showtabs=false,
    breaklines=true,
    showstringspaces=false,
    breakatwhitespace=true,
    basicstyle=\ttfamily\footnotesize,
    frame=l,
    framesep=20pt,
    xleftmargin=12pt,
    tabsize=4,
    captionpos=b}
```

4. Use the language style for example like this:

```java
\begin{lstlisting}[style=cdastyle]
classdiagram Foo {
    
    class NiceClass {
        String niceAttribute;
    }
}
\end{lstlisting}
```